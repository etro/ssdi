addpath('../eccv2004Matlab')
addpath('../SLIC')

Dist = imread(Dist);
Ref = imread(Ref);
[score_loc, score_glob] = SSPD(Ref, Dist);
score_final = 0.05*score_loc + log(score_glob);
