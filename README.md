# SSPD

The project contains the Matlab implementation of the SSPD metric. The SSPD depends on the SLIC and optical flow softwares.

If you use this software for your research, please cite the following paper:

- Saeed Mahmoudpour, and Peter Schelkens, "Synthesized View Quality Assessment Using Feature Matching and Superpixel Difference", IEEE Signal Processing Letters, 2020.
