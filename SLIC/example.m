im =  imread('2.bmp');
%im = double(im);
k=100; %number of superpixels
m=30;% 5 to 40 use large value for more smooth and regular shapes
seRadius=1.5; %1 or 1.5,,0 to disable
colopt = 'mean';

[l, Am, Sp, d] = slic(im, k, m, seRadius, colopt);

imshow(drawregionboundaries(l, im, [255 255 255]))